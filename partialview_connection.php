

<?php 


	// On importe le fichier de configuration
	require_once("config.php");

	// On établie la connexion à la base de données
	$db = new PDO('mysql:host=' . $config['db_host'] . ';dbname=' . $config['db_db'] . ';charset=utf8', $config['db_user'], $config['db_password']);


	// On initialise les booléens correspondant aux checkbox à false
	$secu = false;
	$no_secu = false;
	$table = false;

	// On initialise les résultats des 2 requêtes à null
	$secu_result = null;
	$no_secu_result = null;

	// Si l'utilisateur veut afficher le résultat sécurisé, on met la variable à true
	if(isset($_POST['secu']) and $_POST['secu'] == true) {
		$secu = true;
	}

	// Si l'utilisateur veut afficher le résultat non sécurisé, on met la variable à true
	if(isset($_POST['no_secu']) and $_POST['no_secu'] == true) {
		$no_secu = true;
	}

	// Si l'utilisateur veut afficher le résultat sous forme de tableau, on met la variable à true
	if(isset($_POST['table']) and $_POST['table'] == true) {
		$table = true;
	}


	// Si le formulaire est validé et si le login et le mot de passe sont renseignés
	if(isset($_POST['form']) and isset($_POST['login']) and isset($_POST['password'])){

		// On place les valeurs du login et du mot de passe dans les variables correspondantes
		$login = $_POST['login'];
		$password = $_POST['password'];

		// Formulaire sécurisé
		if($secu){

			// On initialise les variables permettant de savoir si l'utilisateur est connecté et son nombre d'essais restant à false et 3
			$connection = false;
			$trials = 3;

			// On créée la requête pour connaitre le nombre d'essais restant pour le login correspondant
			$sql_trials = "SELECT trials
				FROM users
				WHERE login = :login;";
			$sth_trials = $db->prepare($sql_trials);
			// On prépare la requête pour la sécuriser
			$sth_trials->bindParam(":login", $login, PDO::PARAM_STR, 10);
			// On execute la requête
			$sth_trials->execute();
			// Si on a un résultat
			if(!empty($sth_trials)){
				$trials_result = $sth_trials->fetchAll();
				// Si ce résultat à une ligne
				if(isset($trials_result[0])){
					// On récupère le nombre
					$trials = $trials_result[0]['trials'];
					// Si il reste des essais à l'utilisateur avant que le compte soit bloqué
					if($trials > 0){
						// On créée la requpete pour récupérer les comptes du login
						$sql_secu = "SELECT u.login, a.type, a.amount
							FROM accounts a
							INNER JOIN users u ON a.owner = u.id
							WHERE u.login = :login AND u.password = :password;";
						// On prépare la requête pour la sécuriser
						$sth_secu = $db->prepare($sql_secu);
						// On ajoute les paramètres de façon sécurisé
						$sth_secu->bindParam(":login", $login, PDO::PARAM_STR, 10);
						$sth_secu->bindParam(":password", $password, PDO::PARAM_STR, 50);
						// On execute la requête
						$sth_secu->execute();
						// Si on a un résultat
						if(!empty($sth_secu)){
							$secu_result = $sth_secu->fetchAll();
							// Si le résultat contient au moins 1 ligne
							if(count($secu_result) > 0){
								// On créée un cookie avec les identifiants (pour la faille XSS)
								setcookie("secu_apps", "{login:".$_POST['login'].";password:".$_POST['password']."}", time()+3600);
								// On définit que l'utilisateur est connecté
								$connection = true;
								// On créée la requête pour réinitialiser le nombre d'essais pour le login
								$sql_trials = "UPDATE users
									SET trials = 3
									WHERE login = :login;";
								// On prépare la requête pour la sécuriser
								$sth_trials = $db->prepare($sql_trials);
								// On ajoute les paramètres de façon sécurisé
								$sth_trials->bindParam(":login", $login, PDO::PARAM_STR, 10);
								// On execute la requête
								$sth_trials->execute();
							}
						}
					}
				}
			}
			// Si l'utilisateur n'est pas connecté et qu'il a encore des essais
			if(!$connection and $trials > 0){
				// On créée la requête pour décrémenter de 1 le nombre d'essais pour le login
				$sql_trials = "UPDATE users
					SET trials = trials - 1
					WHERE login = :login;";
				// On prépare la requête pour la sécuriser
				$sth_trials = $db->prepare($sql_trials);
				// On ajoute les paramètres de façon sécurisé
				$sth_trials->bindParam(":login", $login, PDO::PARAM_STR, 10);
				// On execute la requête
				$sth_trials->execute();
				// On décrémente le nombre d'essais
				$trials--;
			}

		}

		// Formulaire non sécurisé
		if($no_secu){

			// On créée la requête de manière non sécurisé
			$sql_no_secu = "SELECT u.login, a.type, a.amount
				FROM accounts a
				INNER JOIN users u ON a.owner = u.id
				WHERE u.login = '$login' AND u.password = '$password';";
			// On l'exécute
			$sth_no_secu = $db->query($sql_no_secu);
			// Si on a n résultat
			if(!empty($sth_no_secu)){
				$no_secu_result = $sth_no_secu->fetchAll();
			}

		}

	}

	// Si le formulaire n'a pas été validé
	if(!isset($_POST['form'])){
		// On initialise les booléen à true
		$secu = true;
		$no_secu = true;
		$table = true;
	}


?>



<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-success">
			<div class="panel-heading text-center">
				<h3>Connexion sécurisé</h3>
			</div>
			<div class="panel-body panel-content-100">
				<?php
					// Si l'utilisateur n'est pas connecté et qu'il a encore des essais
					if(isset($connection) and !$connection and isset($trials) and $trials > 0){
						echo "<h4 class='alert alert-warning text-center'>Erreur : login ou mot de passe incorrect !</h4>";
					}
					// Sinon si l'utilisateur n'a plus des essais
					else if(isset($trials) and $trials == 0){
						echo "<h4 class='alert alert-warning text-center'>Erreur : compte bloqué !</h4>";
					}
					// Sinon si la requête a été exécutée
					else if(isset($sql_secu)){
						// On affiche la requête pour qu'elle s'execute si il y a une faille XSS par exemple (sécurisé donc ça ne le fera pas)
						echo "<div hidden>" . $sql_secu . "</div>";
						// On affiche la requête pour la rendre visible à l'utilisateur
						echo htmlspecialchars($sql_secu);
					}
				?>
			</div>
			<div class="panel-body">
				<?php if($table) : // Si on veut un affichage en tableau ?>
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th>Nom propriétaire</th>
								<th>Type compte</th>
								<th>Montant</th>
							</tr>
						</thead>
						<tbody>
							<?php if(isset($secu_result)) : // Si on a un résultat ?>
								<?php foreach($secu_result as $key => $value) : // Pour tous nos résultats on les affcihes ?>
									<tr>
										<td><?= $value['login'] ?></td>
										<td><?= $value['type'] ?></td>
										<td><?= $value['amount'] ?></td>
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				<?php else : // Si on ne veut pas d'affichage en tableau ?>
					<div>
						<?= var_dump($secu_result); // On affiche la variable ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="panel panel-danger">
			<div class="panel-heading text-center">
				<h3>Connexion non sécurisé</h3>
			</div>
			<div class="panel-body panel-content-100">
				<div hidden> <?php if(isset($sql_no_secu)){ echo $sql_no_secu; } // Si on a un résultat on affiche la requête pour qu'elle s'execute si il y a une faille XSS par exemple ?> </div>
				<?php if(isset($sql_no_secu)){ echo htmlspecialchars($sql_no_secu); } // Si on a un résultat on affiche la requête pour la rendre visible à l'utilisateur ?>
			</div>
			<div class="panel-body">
				<?php if($table) : // Si on veut un affichage en tableau ?>
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th>Nom propriétaire</th>
								<th>Type compte</th>
								<th>Montant</th>
							</tr>
						</thead>
						<tbody>
							<?php if(isset($no_secu_result)) : // Si on a un résultat ?>
								<?php foreach($no_secu_result as $key => $value) : // Pour tous nos résultats on les affcihes ?>
									<tr>
										<td><?= $value['login'] ?></td>
										<td><?= $value['type'] ?></td>
										<td><?= $value['amount'] ?></td>
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				<?php else : // Si on ne veut pas d'affichage en tableau ?>
					<div>
						<?= var_dump($no_secu_result); // On affiche la variable ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>