

<?php


	// On importe le fichier de configuration
	require_once("config.php");


?>


<!DOCTYPE html>
<html lang="fr">
	<head>
		<title><?= $config['title'] ?></title>
		<meta name="description" content="<?= $config['description'] ?>" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="geo.placename" content="Mulhouse" />
		<meta name="geo.position" content="47.731513, 7.314704" />
		<meta name="geo.region" content="FR" />
		<meta name="ICBM" content="47.731513, 7.314704" />
		<meta name="Author" content="<?= $config['author'] ?>" />
		<meta charset="UTF-8" />
		<link rel="icon" type="image/x-icon" href="icon.ico"/>
		<script type="text/javascript" src="jquery/jquery.js"></script>
    	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>	
	    <link rel="stylesheet" href="index.css"/>
		<script type="text/javascript" src="index.js"></script>
		<script type="text/javascript" src="passwords.js"></script>
	</head>
	<body>
		<div class="content">
			<div class="row">
				<div class="col-lg-4" style="">
					<div class="panel-content-300">
						<div class="row">
							<div class="col-lg-6">
								<label class="top-10 left-20"> Faille SQL </label>
								<br />
								<div class="btn-group-vertical top-10">
									<button class="btn btn-default" onclick="failleSQL1()"> Sans mot de passe </button>
									<button class="btn btn-default" onclick="failleSQL2()"> Toutes les données </button>
									<button class="btn btn-default" onclick="failleSQL3()"> Deviner mot de passe </button>
									<button class="btn btn-default" onclick="failleSQL4()"> Afficher informations </button>
									<button class="btn btn-default" onclick="failleSQL5()"> Créer table </button>
									<button class="btn btn-default" onclick="failleSQL6()"> Supprimer table </button>
									<button class="btn btn-default" onclick="failleSQL7()"> Tous les mots de passes </button>
								</div>
							</div>
							<div class="col-lg-6">
								<label class="top-10 left-20"> Normal </label>
								<br />
								<div class="btn-group-vertical top-10">
									<button class="btn btn-default" onclick="normal()"> Normal </button>
								</div>
								<hr />
								<label class="top-10 left-20"> Faille non SQL </label>
								<br />
								<div class="btn-group-vertical top-10">
									<button class="btn btn-default" onclick="failleXSS()"> Faille XSS </button>
									<button class="btn btn-default" onclick="bruteforce()"> Bruteforce </button>
									<button class="btn btn-default" onclick="reinit_bruteforce()"> Réinitialiser </button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="text-center">
						<img src="background-top.jpg" style="height:150px;" />
					</div>
					<div class="panel-content-150">
						<div class="row">
							<div class="col-lg-4 text-right top-5">
								<label for="login" >Login : </label>
							</div>
							<div class="col-lg-8">
								<input id="login" name="login" type="text" placeholder="login" class="form-control" />
							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-lg-4 text-right top-5">
							<label for="password"> Password : </label>
							</div>
							<div class="col-lg-8">
							<input id="password" name="password" type="password" placeholder="password" class="form-control" />
							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-lg-4">
							</div>
							<div class="col-lg-8">
								<input id="form" name="form" type="submit" value="Connexion" class="btn btn-primary" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="panel-content-300">
						<div class="row">
							<div class="col-lg-12">
								<div class="input-group cursor-pointer full-width" onclick="$('#secu').prop('checked', !$('#secu').prop('checked'));">
									<span class="input-group-addon" style="text-align: left; width: 10%;">
										<input id="secu" name="secu" type="checkbox" class="cursor-pointer" onclick="event.stopPropagation();" checked />
									</span>
									<span class="input-group-addon" style="text-align: left;">
										<label class="top-5 cursor-pointer"> Formulaire sécurisé </label>
									</span>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="input-group cursor-pointer full-width" onclick="$('#no_secu').prop('checked', !$('#no_secu').prop('checked'));">
									<span class="input-group-addon" style="text-align: left; width: 10%;">
										<input id="no_secu" name="no_secu" type="checkbox" class="cursor-pointer" onclick="event.stopPropagation();" checked />
									</span>
									<span class="input-group-addon" style="text-align: left;">
										<label class="top-5 cursor-pointer"> Formulaire non sécurisé </label>
									</span>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="input-group cursor-pointer full-width" onclick="$('#table').prop('checked', !$('#table').prop('checked'));">
									<span class="input-group-addon" style="text-align: left; width: 10%;">
										<input id="table" name="table" type="checkbox" class="cursor-pointer" onclick="event.stopPropagation();" checked />
									</span>
									<span class="input-group-addon" style="text-align: left;">
										<label class="top-5 cursor-pointer"> Afficher sous forme de tableau </label>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content" id="div_partialview_connection"> </div>
	</body>
</html>
