

<?php


	// Title of the site
	$config['title'] = "Sécu des Apps";
	// Little description of the site
	$config['description'] = "Projet de sécurisation des applications (GASS - GUNTHER)";
	// Author of the site
	$config['author'] = "Hélène GASS - Sébastien GUNTHER";
	// Host of the database
	$config['db_host'] = "localhost";
	// User of the database
	$config['db_user'] = "root";
	// Password of the user
	$config['db_password'] = "";
	// Database you use
	$config['db_db'] = "securite_bdd";


?>

